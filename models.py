from datetime import datetime

from peewee import *

db = SqliteDatabase('user_bot.sqlite3')


class BaseModel(Model):
    created = DateTimeField(default=datetime.now)

    class Meta:
        database = db


class Groups(BaseModel):
    group_id = IntegerField()
    language = CharField(max_length=3)
    check_username_and_photo = BooleanField(default=False)


class CheckUsernameAndPhotoWarnedUser(BaseModel):
    group = ForeignKeyField(Groups, on_delete='CASCADE', related_name='warned_users')
    user_id = IntegerField()

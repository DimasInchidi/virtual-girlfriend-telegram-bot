#!/usr/bin/env python3
import logging

from decouple import config
from telethon import TelegramClient, events

from models import *

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('telethon').setLevel(level=logging.WARNING)
logging.getLogger('peewee').setLevel(level=logging.WARNING)
logger = logging.getLogger(__name__)

phone_number = config('USER_PHONE_NUMBER')
session_name = 'user_bot_dev'
inchidi_id = 316919926  # 'Inchidi'
client = TelegramClient(
    session_name, config('API_ID', cast=int), config('API_HASH'),
)


def initialize_db():
    db.create_tables(
        [Groups, CheckUsernameAndPhotoWarnedUser, ],
        safe=True
    )


async def add_group(chat_room, language='en', inchidi=None):
    group = Groups.create(
        group_id=chat_room.id,
        language=language
    )
    await client.send_message(
        inchidi,
        f'Group {chat_room.title} with id {chat_room.id} using language {language} added to database'
    )
    return group


@client.on(events.NewMessage)
async def all_message_event_handler(event: events.NewMessage.Event):
    # if its from inchidi
    inchidi = await client.get_entity(inchidi_id)
    if event.message.from_id == inchidi.id:
        if event.raw_text.startswith('!init db'):
            initialize_db()
            await client.send_message(inchidi, 'Database Initialized')
        if event.raw_text.startswith('!turn off room lamp'):
            await event.reply('Your room lamp turned off')
        if event.raw_text.startswith('!turn on room lamp'):
            await event.reply('Your room lamp turned on')
        if event.raw_text.startswith('!delete this'):
            await client.delete_messages(await event.get_chat(), event.message.reply_to_msg_id)
        if event.raw_text.startswith('!check username and photo'):
            chat_id = event.chat_id
            if Groups.select().where(Groups.group_id == chat_id).exists():
                group = Groups.get(Groups.group_id == chat_id)
            else:
                group = await add_group(
                    await event.get_chat(),
                    'id' if event.raw_text.endswith('id') else 'en',
                    inchidi)
            group.check_username_and_photo = True
            group.save()
            await client.send_message(inchidi, f'username and photo member of group {group.group_id} will be checked')


@client.on(events.NewMessage)
async def check_username_and_photo(event: events.NewMessage.Event):
    chat_room = await client.get_entity(event.chat_id)
    group = Groups.select().where(Groups.group_id == chat_room.id)
    if event.message.is_group and group.exists() and not CheckUsernameAndPhotoWarnedUser.select().where(
            CheckUsernameAndPhotoWarnedUser.group == group[0],
            CheckUsernameAndPhotoWarnedUser.user_id == event.message.from_id
    ).exists():
        sender = await event.message.get_sender()
        logger.debug(
            f'message from {sender.first_name} {sender.last_name} ' +
            f'in {chat_room.title} with id {chat_room.id} will be checked, ' +
            f"it has username={bool(str(sender.username))} and photo={bool(sender.photo)}"
        )
        text = ''
        if not str(sender.username) and not sender.photo:
            text = f"you don't have username and pic yet {sender.first_name} 😊" \
                if group[0].language == 'en' else f"belum ada username dan photonya kak {sender.first_name} 😊"
        elif not str(sender.username):
            text = f"you don't have username yet {sender.first_name} 😊" \
                if group[0].language == 'en' else f"belum ada usernamenya kak {sender.first_name} 😊"
        elif not sender.photo:
            text = f"you don't have profile picture yet {sender.first_name}" \
                if group[0].language == 'en' else f"belum ada potonya kak {sender.first_name}"
        if text:
            await client.send_message(
                await client.get_entity(inchidi_id),
                f'''Replying to user with id {sender.id} in
                group {chat_room.title}: {text}'''
            )
            await client.send_message(chat_room, text)
            # await event.reply(text)
            CheckUsernameAndPhotoWarnedUser.create(
                group=group[0],
                user_id=event.message.from_id
            )


initialize_db()
# client.session._conn.execute('delete from entities')
# client.session._conn.commit()
with client.start():
    print('(Press Ctrl+C to stop this)')
    client.run_until_disconnected()
